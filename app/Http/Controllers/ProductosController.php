<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\Productos;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    public function index()
    {
        $productos = Productos::all();
        return view('index', compact('productos'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|max:35',
            'precio' => 'required|integer',
            'cantidad' => 'required|integer',
        ]);

        Productos::create($request->all());
        return redirect()->route('productos.index')->with('success', 'Producto creado exitosamente.');
    }



}
