@extends('layout')

@section('title', 'Lista de Productos')

@section('content')
<div class="bg-white shadow rounded-lg p-6">
    <h1 class="text-2xl font-bold mb-4">Lista de Productos</h1>

    <a href="{{ route('productos.create') }}" class="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-700">Crear Producto</a>



    <table class="min-w-full bg-white mt-4 ">
        <thead class="bg-gray-800 text-white rounded-lg">
            <tr>
                <th class="w-1/4 py-2">ID</th>
                <th class="w-1/4 py-2">Nombre</th>
                <th class="w-1/4 py-2">Precio</th>
                <th class="w-1/4 py-2">Cantidad</th>
                <th class="w-1/4 py-2">Acciones</th>
            </tr>
        </thead>
        <tbody class="text-gray-700 text-center">
            @foreach ($productos as $producto)
                <tr>
                    <td class="w-1/4 py-2">{{ $producto->id_producto }}</td>
                    <td class="w-1/4 py-2">{{ $producto->nombre }}</td>
                    <td class="w-1/4 py-2">{{ $producto->precio }}</td>
                    <td class="w-1/4 py-2">{{ $producto->cantidad }}</td>
                    <td class="w-1/4 py-2 flex gap-2">
                      
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
