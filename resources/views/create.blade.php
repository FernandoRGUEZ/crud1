@extends('layout')

@section('title', 'Crear Producto')

@section('content')
<div class="bg-white shadow rounded-lg p-6">
    <h1 class="text-2xl font-bold mb-4">Crear Producto</h1>

    @if ($errors->any())
        <div class="bg-red-500 text-white p-2 rounded mb-4">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('productos.store') }}" method="POST">
        @csrf
        <div class="mb-4">
            <label for="nombre" class="block text-gray-700">Nombre:</label>
            <input type="text" id="nombre" name="nombre" value="{{ old('nombre') }}" class="w-full p-2 border rounded">
        </div>

        <div class="mb-4">
            <label for="precio" class="block text-gray-700">Precio:</label>
            <input required type="number" id="precio" name="precio" value="{{ old('precio') }}" class="w-full p-2 border rounded">
        </div>

        <div class="mb-4">
            <label for="cantidad" class="block text-gray-700">Cantidad:</label>
            <input required type="number"  id="cantidad" name="cantidad" value="{{ old('cantidad') }}" class="w-full p-2 border rounded">
        </div>

        <button type="submit" class="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-700">Crear</button>
    </form>
</div>
@endsection
