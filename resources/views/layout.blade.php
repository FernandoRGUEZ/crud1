<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title', 'Mi Aplicación')</title>
    <script src="https://cdn.tailwindcss.com"></script>

</head>
    <body class="bg-gray-100 text-gray-800 min-h-screen flex flex-col">
        <header class="bg-blue-600 text-white p-4">
            <nav class="container mx-auto">
                <ul class="flex space-x-4">
                    <li><a href="{{ route('productos.index') }}" class="hover:underline">Inicio</a></li>
                    <li><a href="{{ route('productos.create') }}" class="hover:underline">Crear Producto</a></li>
                </ul>
            </nav>
        </header>

        <main class="flex-grow container mx-auto py-6">
            @yield('content')
        </main>

        <footer class="bg-gray-800 text-white p-4 mt-10">
            <p>&copy; 2024 Mi Aplicación</p>
        </footer>

    </body>
</html>
